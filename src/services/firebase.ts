import firebase from "firebase/app";
import "firebase/auth";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDQGrAF9FXWdZ2IQNJZWzuxnsjv9gUvk74",
  authDomain: "react-shop-edc31.firebaseapp.com",
  databaseURL: "https://react-shop-edc31.firebaseio.com",
  projectId: "react-shop-edc31",
  storageBucket: "react-shop-edc31.appspot.com",
  messagingSenderId: "826750764689",
  appId: "1:826750764689:web:ad2953e90c71ba758b2130",
  measurementId: "G-0ZVWL2J5N7",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const firebaseAuth = firebaseApp.auth();

export const firebaseService = Object.freeze({
  async signInWithGoogle() {
    await firebaseAuth.useDeviceLanguage();
    await firebaseAuth.setPersistence(firebase.auth.Auth.Persistence.LOCAL);
    return await firebaseAuth.signInWithRedirect(
      new firebase.auth.GoogleAuthProvider()
    );
  },

  async getRedirectResult() {
    const result = await firebase.auth().getRedirectResult();
    return result;
  },

  async signOut() {
    return await firebaseAuth.signOut();
  },

  onAuthStateChanged: firebaseAuth.onAuthStateChanged.bind(firebaseAuth),
});
