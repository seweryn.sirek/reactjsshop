import { find } from "lodash";
import { Product, ProductId } from "../../interfaces";
import hardcodedProdcts from "./productHardcoded.json";

export function fetchProducts(
  page: number,
  elementsOnPage = 10
): Promise<Product[]> {
  return Promise.resolve(hardcodedProdcts as Product[]);
}

export function fetchProduct(productId: ProductId): Promise<Product | null> {
  return Promise.resolve(find(hardcodedProdcts, { id: productId }) || null);
}
