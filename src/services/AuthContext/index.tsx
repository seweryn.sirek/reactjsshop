import React, {
  PropsWithChildren,
  createContext,
  useState,
  useMemo,
  useEffect,
  useContext,
} from "react";
import { firebaseService } from "../firebase";

export type User = firebase.User | null;

interface AuthContextProps {
  readonly user?: User;
}

interface AuthContextType {
  readonly user: User;
  readonly setUser: (user: User) => void;
}

export const AuthContext = createContext<AuthContextType>({
  user: null,
  setUser: () => {
    //empty
  },
});

export function useAuthContext() {
  return useContext(AuthContext);
}

export function AuthContextProvider({
  children,
}: PropsWithChildren<AuthContextProps>) {
  const [user, setUser] = useState<User>(null);

  useEffect(() => {
    firebaseService.getRedirectResult().then(({ user }) => setUser(user));

    firebaseService.onAuthStateChanged(setUser);
  }, []);
  const value = useMemo<AuthContextType>(() => ({ user, setUser }), [user]);

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}
