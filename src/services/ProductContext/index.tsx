import React, {
  createContext,
  PropsWithChildren,
  useCallback,
  useContext,
  useMemo,
  useReducer,
} from "react";
import { Product, ProductId } from "../../interfaces";
import { cond, constant, find, matches, noop, random } from "lodash";
import * as productService from "../../services/products";

const INITIAL_STATE: ProductState = {
  products: [],
  isLoading: false,
};

const SET_PRODUCT_ACTION = "SET_PRODUCTS";
const SET_IS_LOADING = "SET_IS_LOADING";

type ProductContextProviderProps = PropsWithChildren<Record<string, unknown>>;

type ProductSetAction = {
  readonly type: "SET_PRODUCTS";
  readonly products: Product[];
};

type IsLoadingSetAction = {
  readonly type: "SET_IS_LOADING";
  readonly isLoading: boolean;
};

type ProductAction = ProductSetAction | IsLoadingSetAction;

interface ProductState {
  readonly products: Product[];
  readonly isLoading: boolean;
}

interface ProductContextType extends ProductState {
  fetchProducts(): void;
  getProduct(id: ProductId): Promise<Product>;
}

export const ProductContext = createContext<ProductContextType>({
  ...INITIAL_STATE,
  fetchProducts: noop,
  getProduct: (id: ProductId) => Promise.reject(),
});

export function useProducts() {
  return useContext(ProductContext);
}

export function ProductContextProvider({
  children,
}: ProductContextProviderProps) {
  const [productState, dispatch] = useReducer(productReducer, INITIAL_STATE);

  const fetchProducts = useCallback(() => {
    dispatch(setIsLoading(true));
    setTimeout(() => {
      productService
        .fetchProducts(0, 10)
        .then((productsFromFetch) => {
          dispatch(setProducts(productsFromFetch));
        })
        .finally(() => {
          dispatch(setIsLoading(false));
        });
    }, random(3000, 7000, false));
  }, [dispatch]);

  const getProduct = useCallback(
    (id: ProductId) => {
      const product = find(productState.products, { id });

      return product ? Promise.resolve(product) : Promise.reject();
    },
    [productState.products]
  );

  const contextValue = useMemo(
    () => ({ ...productState, fetchProducts, getProduct }),
    [productState, fetchProducts, getProduct]
  );

  return (
    <ProductContext.Provider value={contextValue}>
      {children}
    </ProductContext.Provider>
  );
}

function productReducer(state: ProductState, action: ProductAction) {
  return cond([
    [
      matches(SET_PRODUCT_ACTION),
      () => ({
        ...state,
        products: [
          ...(state.products || []),
          ...(action as ProductSetAction).products,
        ],
      }),
    ],
    [
      matches(SET_IS_LOADING),
      () => ({ ...state, isLoading: (action as IsLoadingSetAction).isLoading }),
    ],
    [() => true, constant(state)],
  ])(action.type);
}

function setProducts(products: Product[]): ProductSetAction {
  return {
    type: SET_PRODUCT_ACTION,
    products,
  };
}

function setIsLoading(isLoading: boolean): IsLoadingSetAction {
  return {
    type: SET_IS_LOADING,
    isLoading: isLoading,
  };
}
