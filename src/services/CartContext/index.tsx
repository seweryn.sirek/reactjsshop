import { cond, constant, find, matches, noop } from "lodash";
import React, {
  createContext,
  PropsWithChildren,
  useCallback,
  useContext,
  useMemo,
  useReducer,
} from "react";
import { Product, ProductId } from "../../interfaces";
import { useProducts } from "../ProductContext";

type CartState = Map<ProductId, number>;

type AddToCartAction = { type: "ADD_TO_CART"; productId: ProductId };
type RemoveFromCartAction = { type: "REMOVE_FROM_CART"; productId: ProductId };
type SetAmountAction = {
  type: "SET_AMOUNT";
  productId: ProductId;
  amount: number;
};
type IncrementAmountAction = {
  type: "INCREMENT_AMOUNT";
  productId: ProductId;
};
type DecrementAmountAction = {
  type: "DECREMENT_AMOUNT";
  productId: ProductId;
};

type CartAction =
  | AddToCartAction
  | RemoveFromCartAction
  | SetAmountAction
  | IncrementAmountAction
  | DecrementAmountAction;

type CartContextType = {
  readonly cart: Array<{ product: Product; amount: number }>;
  addToCart(id: ProductId): void;
  removeFromCart(id: ProductId): void;
  isInCart(id: ProductId): boolean;
  increment(id: ProductId): void;
  decrement(id: ProductId): void;
  setAmount(id: ProductId, amount: number): void;
};

const CartContext = createContext<CartContextType>({
  cart: [],
  addToCart: noop,
  removeFromCart: noop,
  isInCart: () => false,
  increment: noop,
  decrement: noop,
  setAmount: noop,
});

export function useCartContext() {
  return useContext(CartContext);
}

export function CartContextProvider({
  children,
}: PropsWithChildren<Record<string, unknown>>) {
  const [cart, dispatch] = useReducer(
    cartReducer,
    new Map<ProductId, number>()
  );

  const { products } = useProducts();

  const addToCart = useCallback(
    (id: ProductId) => dispatch(addToCartAction(id)),
    [dispatch]
  );

  const removeFromCart = useCallback(
    (id: ProductId) => dispatch(removeFromCartAction(id)),
    [dispatch]
  );

  const isInCart = useCallback((id: ProductId) => cart.has(id), [cart]);

  const increment = useCallback(
    (id: ProductId) => {
      dispatch(incrementAmountAction(id));
    },
    [dispatch]
  );

  const decrement = useCallback(
    (id: ProductId) => {
      dispatch(decrementAmountAction(id));
    },
    [dispatch]
  );

  const setAmount = useCallback(
    (id: ProductId, amount: number) => {
      dispatch(setAmountAction(id, amount));
    },
    [dispatch]
  );

  const contextValue = useMemo(
    () => ({
      cart: [...cart.keys()]
        .map((productId) => ({
          product: find(products, { id: productId }) as Product,
          amount: cart.get(productId) || 0,
        }))
        .filter(({ product }) => Boolean(product)),
      addToCart,
      removeFromCart,
      isInCart,
      increment,
      decrement,
      setAmount,
    }),
    [
      cart,
      addToCart,
      removeFromCart,
      isInCart,
      increment,
      decrement,
      setAmount,
      products,
    ]
  );

  return (
    <CartContext.Provider value={contextValue}>{children}</CartContext.Provider>
  );
}

function cartReducer(state: CartState, action: CartAction): CartState {
  return cond([
    [
      matches("ADD_TO_CART"),
      () => {
        const productId = (action as AddToCartAction).productId;

        if (!state.has(productId)) {
          const newState = new Map<ProductId, number>(state);
          newState.set(productId, 1);
          return newState;
        }
        return state;
      },
    ],
    [
      matches("REMOVE_FROM_CART"),
      () => {
        const productId = (action as RemoveFromCartAction).productId;

        if (state.has(productId)) {
          const newState = new Map<ProductId, number>(state);
          newState.delete(productId);
          return newState;
        }
        return state;
      },
    ],
    [
      matches("INCREMENT_AMOUNT"),
      () => {
        const productId = (action as IncrementAmountAction).productId;
        if (state.has(productId)) {
          return new Map<ProductId, number>(state).set(
            productId,
            (state.get(productId) || 0) + 1
          );
        }
        return state;
      },
    ],
    [
      matches("DECREMENT_AMOUNT"),
      () => {
        const productId = (action as DecrementAmountAction).productId;
        if (state.has(productId)) {
          return new Map<ProductId, number>(state).set(
            productId,
            Math.max((state.get(productId) || 0) - 1, 0)
          );
        }
        return state;
      },
    ],
    [
      matches("SET_AMOUNT"),
      () => {
        const { productId, amount } = action as SetAmountAction;
        if (state.has(productId)) {
          return new Map<ProductId, number>(state).set(
            productId,
            Math.max(amount, 0)
          );
        }
        return state;
      },
    ],
    [() => true, constant(state)],
  ])(action.type);
}

function addToCartAction(productId: ProductId): AddToCartAction {
  return { type: "ADD_TO_CART", productId };
}

function removeFromCartAction(productId: ProductId): RemoveFromCartAction {
  return { type: "REMOVE_FROM_CART", productId };
}

function incrementAmountAction(productId: ProductId): IncrementAmountAction {
  return { type: "INCREMENT_AMOUNT", productId };
}

function decrementAmountAction(productId: ProductId): DecrementAmountAction {
  return { type: "DECREMENT_AMOUNT", productId };
}

function setAmountAction(
  productId: ProductId,
  amount: number
): SetAmountAction {
  return { type: "SET_AMOUNT", productId, amount };
}
