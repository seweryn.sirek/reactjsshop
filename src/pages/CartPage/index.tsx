import React from "react";
import { Container, Typography, Paper, makeStyles } from "@material-ui/core";
import { Cart } from "../../components/Cart";

export function CartPage() {
  const classes = useStyles();

  return (
    <Container maxWidth="lg">
      <Paper className={classes.paper}>
        <Typography component="h1" variant="h5" className={classes.title}>
          Cart
        </Typography>
      </Paper>
      <Paper>
        <Cart isReadOnly={false} />
      </Paper>
    </Container>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(1),
    padding: theme.spacing(10),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  title: {
    marginBottom: theme.spacing(2),
  },
  productCard: {
    marginBottom: theme.spacing(2),
    maxWidth: "300px",
  },
}));
