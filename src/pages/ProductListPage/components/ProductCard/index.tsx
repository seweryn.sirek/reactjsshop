import { Paper, Typography, Button } from "@material-ui/core";
import React, { useMemo } from "react";
import { Product, ProductId } from "../../../../interfaces";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import noop from "lodash/noop";
import { AddToCartButton, ProductAvatarItem } from "../../../../components";

type ProductCallback = (id: ProductId) => void;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    container: {
      padding: theme.spacing(2),
      cursor: "pointer",
    },
    paper: {
      padding: theme.spacing(2),
      margin: "auto",
      maxWidth: 500,
    },
    image: {
      width: 128,
      height: 128,
    },
    img: {
      margin: "auto",
      display: "block",
      maxWidth: "100%",
      maxHeight: "100%",
    },
  })
);

interface ProductCardProps extends Product {
  readonly className?: string;
  readonly onAddToCardClick?: ProductCallback;
  readonly onShowProductClick?: ProductCallback;
}

export function Test(): number {
  return 1;
}

export function ProductCard({
  id,
  className,
  name,
  price,
  shortDescription,
  miniature,
  onShowProductClick = noop,
}: ProductCardProps) {
  const classes = useStyles();

  const handleShowProductClicked = useMemo(
    () =>
      eventWrapper<HTMLButtonElement | HTMLDivElement>(id, onShowProductClick),
    [id, onShowProductClick]
  );

  return (
    <Paper
      onClick={handleShowProductClicked}
      elevation={3}
      className={`${classes.container} ${className}`}
    >
      <ProductAvatarItem productId={id} />
      <Typography variant="h5" component="h2">
        {name}
      </Typography>
      <Typography>{shortDescription}</Typography>
      <Typography>{price.toFixed(2)}</Typography>
      <Button onClick={handleShowProductClicked}>Show product</Button>
      <AddToCartButton productId={id} />
    </Paper>
  );
}

function eventWrapper<E>(id: ProductId, callback: ProductCallback) {
  return (e: React.MouseEvent<E>) => {
    e.preventDefault();
    e.stopPropagation();
    callback(id);
  };
}
