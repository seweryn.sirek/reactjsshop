import React, { useCallback, useEffect } from "react";
import {
  Container,
  Paper,
  Typography,
  makeStyles,
  CircularProgress,
} from "@material-ui/core";
import { ProductId } from "../../interfaces";
import { ProductCard } from "./components";
import { useProducts } from "../../services";
import { useHistory } from "react-router-dom";
import { Cart } from "../../components/Cart";

export function ProductListPage() {
  const classes = useStyles();
  const history = useHistory();
  const { products, isLoading, fetchProducts } = useProducts();

  useEffect(() => {
    fetchProducts();
  }, [fetchProducts]);

  const handleShowProductClicked = useCallback(
    (id: ProductId) => {
      history.push(`/product/${id}`);
    },
    [history]
  );

  return (
    <Container maxWidth={"lg"}>
      <Paper className={classes.paper}>
        <Typography component="h1" variant="h5" className={classes.title}>
          Our products
        </Typography>
        {products?.length === 0 && <Typography>No products</Typography>}
        {products?.length > 0 &&
          products.map((product) => (
            <ProductCard
              {...product}
              key={product.id}
              className={classes.productCard}
              onShowProductClick={handleShowProductClicked}
            />
          ))}
        {isLoading && <CircularProgress />}
      </Paper>
      <Paper>
        <Cart isReadOnly={false} />
      </Paper>
      <Paper>
        <Cart isReadOnly={true} />
      </Paper>
    </Container>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(1),
    padding: theme.spacing(10),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  title: {
    marginBottom: theme.spacing(2),
  },
  productCard: {
    marginBottom: theme.spacing(2),
    maxWidth: "300px",
  },
}));
