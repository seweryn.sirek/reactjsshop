import {
  CircularProgress,
  Container,
  makeStyles,
  Paper,
  Typography,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { Redirect, useParams } from "react-router-dom";
import { AddToCartButton } from "../../components";
import { Product } from "../../interfaces";
import { fetchProduct } from "../../services/products";

export function ProductPage() {
  const classes = useStyles();
  const { productId } = useParams<{ productId: string }>();

  const [product, setProduct] = useState<Product | null | undefined>(undefined);

  useEffect(() => {
    fetchProduct(productId).then(setProduct);
  }, [setProduct, productId]);

  if (undefined === product) {
    return <CircularProgress />;
  }

  if (null === product) {
    return <Redirect to="/" />;
  }

  const { name, price, id, shortDescription, description, image } = product;

  return (
    <Container maxWidth={"lg"}>
      <Paper className={classes.paper}>
        <Typography component="h1" variant="h5">
          {name}
        </Typography>
        <Typography>{description}</Typography>
        <Typography>{price.toFixed(2)}</Typography>
        <img src={image} alt={shortDescription}></img>
        <AddToCartButton productId={id} />
      </Paper>
    </Container>
  );
}

const useStyles = makeStyles(() => ({
  paper: {},
}));
