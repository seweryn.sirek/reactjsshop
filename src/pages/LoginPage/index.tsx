import React, { useCallback } from "react";
import { Redirect } from "react-router-dom";
import { Container, Typography, Paper, makeStyles } from "@material-ui/core";
import GoogleButton from "react-google-button";
import { firebaseService } from "../../services/firebase";
import { useAuthContext } from "../../services/AuthContext";

export function LoginPage() {
  const classes = useStyles();
  const { user } = useAuthContext();

  const handleClickLoginButton = useCallback(() => {
    firebaseService.signInWithGoogle();
  }, []);

  if (null !== user) {
    return <Redirect to="/" />;
  }
  return (
    <Container maxWidth="sm">
      <Paper className={classes.paper}>
        <Typography component="h1" variant="h5">
          Log in
        </Typography>

        <GoogleButton
          onClick={handleClickLoginButton}
          className={classes.loginButton}
        />
      </Paper>
    </Container>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    padding: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
  },
  loginButton: {
    marginTop: theme.spacing(2),
  },
}));
