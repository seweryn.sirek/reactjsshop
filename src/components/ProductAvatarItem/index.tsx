import { Avatar, CircularProgress } from "@material-ui/core";
import { BrokenImage } from "@material-ui/icons";
import React, { useEffect, useState } from "react";
import { Product, ProductId } from "../../interfaces";
import { useProducts } from "../../services";

interface ProductAvatarItemProps {
  readonly productId: ProductId;
  readonly className?: string;
}

export function ProductAvatarItem({
  productId,
  className,
}: ProductAvatarItemProps) {
  const { getProduct } = useProducts();

  const [product, setProduct] = useState<undefined | null | Product>(undefined);

  useEffect(() => {
    getProduct(productId).then(setProduct, setProduct.bind(null, null));
  }, [getProduct, productId]);

  if (product === undefined) {
    return <CircularProgress />;
  }

  return (
    <Avatar
      src={product ? product.miniature : undefined}
      alt={product ? product.description : ""}
      className={className}
    >
      <BrokenImage />
    </Avatar>
  );
}
