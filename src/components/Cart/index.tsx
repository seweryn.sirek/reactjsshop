import { Grid } from "@material-ui/core";
import React from "react";
import { useCartContext } from "../../services";
import { ProductAvatarItem } from "../ProductAvatarItem";
import { CartItemControls } from "./components";

interface CartProps {
  readonly isReadOnly: boolean;
}

export function Cart({ isReadOnly }: CartProps) {
  const { cart } = useCartContext();

  return (
    <>
      {" "}
      {cart.map(({ product, amount }) => (
        <Grid
          key={product.id}
          container
          direction="row"
          justify="flex-start"
          alignItems="center"
        >
          <Grid item>
            <ProductAvatarItem productId={product.id} />
          </Grid>
          <Grid item>
            {product.name} - {product.shortDescription}
          </Grid>
          <Grid item>
            {isReadOnly ? (
              <> || Amount: {amount}</>
            ) : (
              <CartItemControls productId={product.id} amount={amount} />
            )}
          </Grid>
        </Grid>
      ))}
    </>
  );
}
