import { Button, TextField } from "@material-ui/core";
import { Add, Remove } from "@material-ui/icons";
import React, { useCallback } from "react";
import { ProductId } from "../../../../interfaces";
import { useCartContext } from "../../../../services";
import { RemoveFromCartButton } from "../../../RemoveFromCartButton";

interface CartItemControlsProps {
  readonly productId: ProductId;
  readonly amount: number;
}

export function CartItemControls({ productId, amount }: CartItemControlsProps) {
  const { increment, decrement, setAmount } = useCartContext();

  const handleReduceClick = useCallback(
    (e: React.MouseEvent<HTMLButtonElement>) => {
      e.preventDefault();
      decrement(productId);
    },
    [decrement, productId]
  );

  const handleIncreaseClick = useCallback(
    (e: React.MouseEvent<HTMLButtonElement>) => {
      e.preventDefault();
      increment(productId);
    },
    [increment, productId]
  );

  const handleAmountChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setAmount(productId, Number.parseInt(e.currentTarget.value));
    },
    [productId, setAmount]
  );

  return (
    <>
      <Button onClick={handleReduceClick}>
        <Remove />
      </Button>
      <TextField value={amount} onChange={handleAmountChange} />
      <Button onClick={handleIncreaseClick}>
        <Add />
      </Button>
      <RemoveFromCartButton productId={productId} />
    </>
  );
}
