import { Button } from "@material-ui/core";
import React, { useCallback } from "react";
import { ProductId } from "../../interfaces";
import { RemoveCircle } from "@material-ui/icons";
import { useCartContext } from "../../services";

interface RemoveFromCartButtonProps {
  readonly productId: ProductId;
  readonly className?: string;
}

export function RemoveFromCartButton({
  productId,
  className,
}: RemoveFromCartButtonProps) {
  const { removeFromCart, isInCart } = useCartContext();

  const handleClick = useCallback(
    (e: React.MouseEvent) => {
      e.preventDefault();
      e.stopPropagation();

      removeFromCart(productId);
    },
    [productId, removeFromCart]
  );

  return (
    <>
      <Button
        variant="contained"
        color="secondary"
        className={className}
        onClick={handleClick}
        startIcon={<RemoveCircle />}
        disabled={!isInCart(productId)}
      >
        Remove from card
      </Button>
    </>
  );
}
