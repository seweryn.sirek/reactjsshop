import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import {
  CartPage,
  LoginPage,
  ProductListPage,
  ProductPage,
} from "../../../../pages";
import { useAuthContext } from "../../../../services";

export function AppRoutes() {
  const { user } = useAuthContext();

  return (
    <Switch>
      <Route path="/" exact component={ProductListPage} />
      <Route path="/login" component={LoginPage} />
      <Route path="/login" component={LoginPage} />
      {null !== user && <Route path="/cart" component={CartPage} />}
      <Route path="/product/:productId" component={ProductPage} />
      <Redirect to="/" from="/products" />
    </Switch>
  );
}
