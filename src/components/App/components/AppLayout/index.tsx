import { Container, CssBaseline, makeStyles, Theme } from "@material-ui/core";
import React, { PropsWithChildren } from "react";
import { Footer, TopBar } from "./components";

type AppLayout = PropsWithChildren<Record<string, unknown>>;

export function AppLayout({ children }: AppLayout) {
  const classes = useStyles();

  return (
    <>
      <TopBar />
      <Container maxWidth="lg" component="main" className={classes.main}>
        <CssBaseline />
        {children}
      </Container>
      <Footer />
    </>
  );
}

const useStyles = makeStyles((theme: Theme) => ({
  main: {
    flexGrow: 1,
    flexShrink: 0,
  },
}));
