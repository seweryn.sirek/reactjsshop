import React, { useCallback, useContext, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import { useHistory } from "react-router-dom";
import { AuthContext } from "../../../../../../services/AuthContext";
import { CartMenuButton, UserMenu } from "./components";
import { MenuItem } from "@material-ui/core";

export function TopBar() {
  const history = useHistory();
  const { user, setUser } = useContext(AuthContext);

  useEffect(() => {
    setTimeout(() => {
      console.log("set To null");
      setUser(null);
    }, 3000);
  }, [setUser]);

  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }));
  const classes = useStyles();

  const handleLoginButtonClick = useCallback(() => {
    history.push("/login");
  }, [history]);

  const handleCartMenuButtonClick = useCallback(() => {
    history.push("/cart");
  }, [history]);

  const handleMenuClick = useCallback(() => {
    console.log("menu");
  }, []);

  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
            onClick={handleMenuClick}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            News
          </Typography>

          {null !== user && (
            <MenuItem>
              <CartMenuButton onClick={handleCartMenuButtonClick} />
            </MenuItem>
          )}
          {null === user && (
            <Button color="inherit" onClick={handleLoginButtonClick}>
              Login
            </Button>
          )}
          {null !== user && <UserMenu />}
        </Toolbar>
      </AppBar>
    </>
  );
}
