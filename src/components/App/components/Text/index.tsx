import React from "react";

// interface TextProps {
//   readonly onLinkClicked: (e?: MouseEvent<HTMLAnchorElement>) => void;
// }

export function Text() {
  return <p>TEXT</p>;
}

// export function Text({ onLinkClicked }: TextProps) {
//   return (
//     <>
//       <p>
//         Edit <code>src/App.tsx</code> and save to reload.
//       </p>
//       <a
//         onClick={onLinkClicked}
//         className="App-link"
//         href="https://reactjs.org"
//         target="_blank"
//         rel="noopener noreferrer"
//       >
//         Learn React
//       </a>
//     </>
//   );
// }
