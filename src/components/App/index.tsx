import React, { useEffect } from "react";
import "./App.css";
import { firebaseService } from "../../services/firebase";
import { AppContexts, AppLayout, AppRoutes } from "./components";

export function App() {
  useEffect(() => {
    firebaseService.getRedirectResult();
    firebaseService.onAuthStateChanged((result) => {
      console.log("onAuthStateChanged", result);
    });
  }, []);

  return (
    <AppContexts>
      <AppLayout>
        <AppRoutes />
      </AppLayout>
    </AppContexts>
  );
}
