export type ProductId = string;
type ProductName = string;
type ProductPrice = number;
type ProductMiniatureURL = string;
type ProductImageURL = string;

export interface Product {
  readonly id: ProductId;
  readonly name: ProductName;
  readonly price: ProductPrice;
  readonly shortDescription: string;
  readonly description: string;
  readonly miniature?: ProductMiniatureURL;
  readonly image?: ProductImageURL;
}
